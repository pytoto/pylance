.PHONY: clean-pyc clean-build docs clean

help:
	@echo "clean       - remove all build, test, coverage and Python runtimes (may need sudo)"
	@echo "clean-build - remove build artifacts (may need sudo)"
	@echo "clean-pyc   - remove Python runtime artifacts"
	@echo "clean-test  - remove test and coverage artifacts"
	@echo "lint        - check style with flake8"
	@echo "test        - run tests quickly with the default Python"
	@echo "test-all    - run tests on every Python version with tox"
	@echo "coverage    - check code coverage quickly with the default Python"
	@echo "release     - package and upload a release"
	@echo "dist        - package"
	@echo "install     - install the package to the active Python's site-packages"
	@echo "stats       - count files and lines of .py files"
	@echo "upload      - upload package to devpi repo"

clean: clean-build clean-pyc clean-test

clean-build:
	rm -fr build/
	rm -fr dist/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -fr {} +

clean-pyc:
	find . -name '*.pyc' -delete
	find . -name '__pycache__' -exec rm -fr {} +

clean-test:
	rm -fr .tox/
	rm -f tests/.coverage .coverage
	rm -fr htmlcov/

requirements: ## Install requirements
	pip install -r requirements.txt
	pip install -r tests/requirements.txt

lint:
	flake8 pylance tests scripts

pycodestyle:
	pycodestyle pylance tests scripts

pylint:
	pylint --rcfile=.pylintrc pylance tests scripts

test:
	python setup.py test

check:
	find . -type f -name "*.py" -exec grep -l "print" {} +
	find . -type f -name "*.py" -exec grep -l "set_trace()" {} +

test-all:
	tox

coverage:
	coverage run --source pylance setup.py test
	coverage report -m
	coverage html
	open htmlcov/index.html

release: clean
	python setup.py sdist upload
	python setup.py bdist_wheel upload

dist: clean
	python setup.py sdist
	python setup.py bdist_wheel
	ls -l dist

install: clean
	python setup.py install

stats:
	@echo `find . -name '*.py' | wc -l` 'files'
	@echo `(find ./ -name '*.py' -print0 | xargs -0 cat ) | wc -l` 'lines'

upload:
	devpi login root --password=$(PASSWORD)
	devpi use root/stage
	devpi upload

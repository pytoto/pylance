Pylance - an automatic expenses balance script
==============================================

Python script to balance expenses after a trip or week end with friends.

Allows everyone to expense for the whole group knowing that he will get a quick and accurate balance at the end.
The scripts calculates who must get money back and who must give money, the exact amounts to transfer and optimizes the number of money transfers.


Quickstart
----------

Install Pylance:
```
python setup.py install
```

Run Pylance:
```
pylance
```

Launch tests:
```
pip install -r tests/requirements.txt
make test
make coverage
```

For more details, see docs/documentation.md

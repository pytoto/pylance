# encoding: utf8

from setuptools import setup

from pylance.utils import root_file
from pylance import __version__


def read_file(file):
    try:
        with open(root_file(__file__, file), 'r') as f:
            return f.read().strip()
    except:
        return ''

README = read_file('README.md')
CHANGELOG = read_file('CHANGELOG.txt')
REQUIREMENTS = read_file('requirements.txt').split('\n')
TEST_REQUIREMENTS = read_file('tests/requirements.txt').split('\n')

setup(
    name='pylance',
    version=__version__,
    description='Balance expenses after a trip or week end with friends',
    long_description='\n' + '\n'.join([README, CHANGELOG]) + '\n',
    author="François Vincent",
    author_email='francois.vincent01@gmail.com',
    maintainer='François Vincent',
    url='https://gitlab.com/pytoto/pylance',
    packages=['pylance'],
    install_requires=REQUIREMENTS,
    license="MIT",
    keywords='utility',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Environment :: Console',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: Implementation :: CPython',
        'Operating System :: POSIX',
    ],
    test_suite='tests',
    tests_require=TEST_REQUIREMENTS,
    entry_points={'console_scripts': [
        'pylance = pylance.commands:script',
    ]}
)

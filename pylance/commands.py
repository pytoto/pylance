# coding: utf-8

from __future__ import print_function
import decimal
from decimal import Decimal
from functools import partial

from colored import fg, bg, attr


class CO(object):
    def __init__(self, default_bg=0):
        self.default_bg = bg(default_bg)
    def format(self, color, string):
        return "%s%s%s" % (color, string, attr(0))
    def __getattr__(self, color):
        try:
            bg_color, fg_color = color.split('__')
            return partial(self.format, bg(bg_color) + fg(fg_color))
        except ValueError:
            return partial(self.format, self.default_bg + fg(color))

co = CO()
WARNING = co.yellow
CREDITOR_COLOR = co.yellow_4b
DEBTOR_COLOR = co.medium_orchid


def max_item_value(data):
    max_key, max_value = None, Decimal(0)
    for k, v in data.iteritems():
        if v > max_value:
            max_key, max_value = k, v
    return max_key, max_value


def transfer_iteration(creditors, debtors):
    cred_key, cred_val = max_item_value(creditors)
    debt_key, debt_val = max_item_value(debtors)
    if cred_val >= debt_val:
        del debtors[debt_key]
        creditors[cred_key] -= debt_val
        return cred_key, debt_key, debt_val
    else:
        debtors[debt_key] -= cred_val
        del creditors[cred_key]
        return cred_key, debt_key, cred_val


def execute(precision=2):
    decimal.getcontext().prec = precision + 1
    # input data
    print("Enter each participant name and expense. Hit return to end input")
    data = {}
    while True:
        line = raw_input("  enter participant %d (name:expense) " % (len(data) + 1,))
        if not line:
            break
        name, amount = line.split(':')
        if name in data:
            print(WARNING("  participant %s already exists, please choose another name" % name))
        else:
            try:
                data[name] = Decimal(amount)
            except decimal.InvalidOperation:
                print(WARNING("  this amount is not correct, please enter an amount like 12.50"))
    if not len(data):
        return

    # calculate sum and mean
    print("\nThank you, calculating basics...")
    print("There are %d participants" % len(data))
    total = sum(data.values())
    print("The total amount is %s" % total)
    mean = total / len(data)
    print("The mean amount is %s per person\n" % mean)

    # show creditors and debtors with colors
    creditors, debtors = {}, {}
    for name, amount in data.iteritems():
        this_diff = amount - mean
        if this_diff > 0:
            print("Participant " + CREDITOR_COLOR(name) + " must receive %s" % this_diff)
            creditors[name] = this_diff
        if this_diff < 0:
            print("Participant " + DEBTOR_COLOR(name) + " must give %s" % (-this_diff,))
            debtors[name] = -this_diff

    # calculate transfers
    print("\nCalculating transfers...")
    _creditors, _debtors = dict(creditors), dict(debtors)
    while len(_creditors) and len(_debtors):
        cred, debt, amount = transfer_iteration(_creditors, _debtors)
        print("transfer %s from %s to %s" % (amount, DEBTOR_COLOR(debt), CREDITOR_COLOR(cred)))

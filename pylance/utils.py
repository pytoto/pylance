# coding: utf-8

import os


def root_file(path, file_spec):
    if os.path.isfile(path):
        path = os.path.dirname(path)
    return os.path.join(path, file_spec)
